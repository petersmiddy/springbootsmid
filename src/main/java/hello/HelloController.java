package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {
    
    @RequestMapping("/hello")
    public String index() {
        return "Greetings Java Master!";
    }

    @RequestMapping("/get_time")
    public String time() {
    	long millis=System.currentTimeMillis();
    	java.util.Date date=new java.util.Date(millis);
    	return date.toString();
    }

    
}
